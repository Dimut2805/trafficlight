package com.example.trafficlight;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    LinearLayout linearLayout;
    private int backgroundLayout = Color.WHITE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linearLayout = (findViewById(R.id.linearLayout));
        if (savedInstanceState != null) {
            backgroundLayout = savedInstanceState.getInt("backgroundLayout");
        }
        linearLayout.setBackgroundColor(backgroundLayout);
    }
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("backgroundLayout", backgroundLayout);
    }

    public void onClick(View view) {
        backgroundLayout = ((Button) findViewById(view.getId())).getCurrentTextColor();
        linearLayout.setBackgroundColor(backgroundLayout);

    }
}